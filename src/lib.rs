#[cfg(test)]
mod tests {
  use self::super::ConsumeContext;
  use serde::Serialize;

  #[derive(Serialize)]
  pub struct Wisdom {
    fact: String
  }

  #[tokio::test]
  async fn can_publish() {
    let context = ConsumeContext::from_message(Wisdom { fact: "All cops are bastards".to_string() }).await.unwrap();
    context.publish(&Wisdom { fact: "Stonks".to_string() }).await.unwrap();
  }
}

use async_trait::async_trait;
use lapin::{BasicProperties, Channel, Connection, ConnectionProperties};
use lapin::options::{BasicPublishOptions};
use lapin::publisher_confirm::Confirmation::*;
use serde::Serialize;

pub struct ConsumeContext<TMessage> {
  pub message: TMessage,
  channel: Channel
}

impl<T> ConsumeContext<T> {
  pub async fn from_message(message: T) -> lapin::Result<Self> {
    let addr = std::env::var("AMQP_ADDR").unwrap_or_else(|_| "amqp://127.0.0.1:5672/%2f".into());
    let conn = Connection::connect(
      &addr,
      ConnectionProperties::default().with_default_executor(8),
    ).await?;

    Ok(ConsumeContext {
      message: message,
      channel: conn.create_channel().await?
    })
  }
}

#[async_trait]
pub trait Consumer
{
  async fn consume<TMessage>(context: ConsumeContext<TMessage>) -> lapin::Result<()>;
}

#[derive(thiserror::Error, Debug)]
pub enum PublishError {
  #[error("message nacked")]
  Nacked,
  #[error("amqp error")]
  Lapin(#[from] lapin::Error),
  #[error("message serialization error")]
  JsonSerializationError(#[from] serde_json::Error)
}

impl<TMessage> ConsumeContext<TMessage> {
  pub async fn publish<TPublishMessage: Serialize>(&self, message: &TPublishMessage) -> Result<(), PublishError> {
    let payload = serde_json::to_vec(message)?;
    let exchange = self.ensure_exchange::<TPublishMessage>().await?;

    let confirm = self.channel
      .basic_publish(
        exchange,
        "",
        BasicPublishOptions::default(),
        payload,
        BasicProperties::default(),
      )
      .await?
      .await?;

    match confirm {
      Nack(_) => Err(PublishError::Nacked),
      _ => Ok(())
    }
  }

  async fn ensure_exchange<T>(&self) -> lapin::Result<&'static str> {
    let exchange = std::any::type_name::<T>();
    let queue = std::any::type_name::<T>();

    self.channel.exchange_declare(
      exchange,
      lapin::ExchangeKind::Direct,
      lapin::options::ExchangeDeclareOptions::default(),
      amq_protocol_types::FieldTable::default())
      .await
      .expect("Unable to declare exchange");


    self.channel.queue_declare(
      queue,
      lapin::options::QueueDeclareOptions::default(),
      amq_protocol_types::FieldTable::default())
      .await
      .expect("Unable to declare queue");

    self.channel.queue_bind(
      queue,
      exchange,
      "",
      lapin::options::QueueBindOptions::default(),
      amq_protocol_types::FieldTable::default())
      .await
      .expect("Unable to bind queue to exchange");

    Ok(exchange)
  }
}
